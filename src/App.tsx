import React from 'react';
import './App.css';
import { TextField } from './TextField';
import { ReducerExample } from './ReducerExample';
import { Counter } from './Counter';


const App: React.FC = () => {
  return <div>
    <TextField text="asdf" handleChange={(e) => { }}></TextField>
    <Counter>
      {(count, setCount) => (
        <div>
          {count}
          <button onClick={() => setCount(count + 1)}>+</button>
        </div>
      )}
    </Counter>
    <ReducerExample />
  </div>;
}

export default App;
