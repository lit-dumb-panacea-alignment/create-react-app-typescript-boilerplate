import React, { useState, useRef } from 'react'

interface Props {
	text: string
	ok?: boolean
	i?: number
	fn?: (bob: string) => string
	handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

export const TextField: React.FC<Props> = ({ handleChange }) => {
	const [count, setCount] = useState<{ text: string }>({ text: '5' })
	const inputRef = useRef<HTMLInputElement>(null)
	const divRef = useRef<HTMLDivElement>(null)

	let { text } = count

	return (
		<div ref={divRef}>
			<input ref={inputRef} onChange={handleChange} value={text} />
			<button onClick={() => {
				setCount({ text: 'nut' })
			}}>
				Click
			</button>
		</div>
	)
}